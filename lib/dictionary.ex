defmodule Dictionary do
  @moduledoc """
  Documentation for Dictionary.
  """

  defdelegate start(), to: Dictionary.WordList, as: :start_link
  defdelegate random_word(), to: Dictionary.WordList

end
