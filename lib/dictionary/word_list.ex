defmodule Dictionary.WordList do

  @file_path "../../assets/words.txt"
  @me __MODULE__

  def start_link do
   Agent.start_link(&word_list/0, name: @me)
  end

  def word_list do
    @file_path
    |> Path.expand(__DIR__)
    |> File.read!
    |> String.split(~r/\n/)
  end

  def random_word() do
    Agent.get(@me, &Enum.random/1)
  end
end
